package me.expdev.testplugin;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by mariusri.
 * As of 22.05.2017, this class was created.
 */
public class Journey {

    private Set<String> messages = new LinkedHashSet<String>();

    public Journey(String... messages) {
        Collections.addAll(this.messages, messages);
    }

    public Set<String> getMessages() {
        return messages;
    }
}
