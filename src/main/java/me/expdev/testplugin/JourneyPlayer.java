package me.expdev.testplugin;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by mariusri.
 * As of 22.05.2017, this class was created.
 */
public class JourneyPlayer {

    // Needed for schedulers
    private JavaPlugin plugin;

    // Data
    private Player player;
    private Journey journey;

    // Contains all of the tasks associated with this player
    private Set<Integer> tasks = new LinkedHashSet<Integer>();

    /**
     *
     * @param plugin Plugin to handle the schedulers
     * @param player The bukkit Player the Journey is for
     */
    public JourneyPlayer(JavaPlugin plugin, Player player) {
        this.plugin = plugin;
        this.player = player;
    }

    /**
     *
     * @param journey The Journey to associate with
     * @return this with added Journey
     */
    public JourneyPlayer withJourney(Journey journey) {
        this.journey = journey;
        return this;
    }

    /**
     * Play the journey
     *
     * @return Success state
     */
    public boolean play() {
        Player player = this.player;
        if (player == null) {
            return false;
        }

        // When should task run?
        long ticksToNextTask = -1L;
        for (final String s : this.journey.getMessages()) {
            if (ticksToNextTask < 0) {
                // First message. Instantly send
                player.sendMessage(s);

                // Now we should be able to get past this
                ticksToNextTask = 0;

                // Continue to next iteration
                continue;
            }

            // All characters in message
            char[] chars = s.toCharArray();

            // Calculating time
            ticksToNextTask+= chars.length * 10L;

            // Scheduling task to run
            this.tasks.add(
                    Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, () -> {
                        player.sendMessage(s);
                    }, ticksToNextTask)
            );
        }

        return true;
    }

    /**
     * Stop the journey
     */
    public void stop() {
        // Stopping all tasks assosiated with this player
        this.tasks.forEach(id -> {
            if (Bukkit.getScheduler().isQueued(id)) {
                // Cancel only if it's queued
                Bukkit.getScheduler().cancelTask(id);
            }
        });
    }

}
