package me.expdev.testplugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by mariusri.
 * As of 27.04.2017, this class was created.
 */
public class TestCommandCommand implements CommandExecutor {

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        // Only players can start journeys
        if (!(commandSender instanceof Player)) {
            return true;
        }

        // Our bukkit Player
        Player player = (Player) commandSender;

        // Building a Journey Player, maybe you
        // want another system for this
        JourneyPlayer journeyPlayer = new JourneyPlayer(TestPluginPlugin.p, player)
                .withJourney(new Journey(
                        ChatColor.AQUA + "Hello!",
                        ChatColor.RED + "Welcome to mc.server.com",
                        ChatColor.GREEN + "Visit our website at www.server.com",
                        ChatColor.LIGHT_PURPLE + "Enjoy your stay here, I am sure you will enjoy this super long message",
                        ChatColor.DARK_RED + "End of journey :)"
                ));

        // Play the journey!
        journeyPlayer.play();

        // Hmm, maybe some condition happened? Stop the journey in 3 seconds
        // (guaranteed it won't have ended)
        Bukkit.getScheduler().scheduleSyncDelayedTask(TestPluginPlugin.p, journeyPlayer::stop, 20 * 3L);
        return true;
    }
}
