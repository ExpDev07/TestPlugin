package me.expdev.testplugin;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by mariusri.
 * As of 27.04.2017, this class was created.
 */
public class TestPluginPlugin extends JavaPlugin {

    public static TestPluginPlugin p;

    @Override
    public void onLoad() {
        p = this;
    }

    @Override
    public void onDisable() {

    }

    @Override
    public void onEnable() {
        getCommand("").setExecutor(new TestCommandCommand());
    }
}
